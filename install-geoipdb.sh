#!/bin/bash

if [ -d "$(readlink -f data)" ] && ! [[ -n $(find /usr/share/GeoIP -newer "$(readlink -f data)") ]]; then
  echo "GeoIPDB is up-to-date."
  exit 0
fi

mkdir -p tmp/GeoIPDB
cd tmp/GeoIPDB/

if [ -d /usr/share/GeoIP ]; then
  echo "Checking out latest GeoIPDB from /usr/share/GeoIP..."
  cp /usr/share/GeoIP/* .
else
  echo "Checking out latest GeoIPDB from Dynamic Yield servers..."
  exit 1
  git archive -v --format=tar.gz -o GeoIPDB.tar.gz --remote=git@bitbucket.org:DynamicYield/GeoIPDB.git master 2>&1
  tar xzf GeoIPDB.tar.gz
  rm GeoIPDB.tar.gz
fi

echo "Installing latest GeoIPDB..."
ls *.zip | xargs -I{} unzip "{}"
mv "$(find . -name "*-Location.csv")" ../GeoLiteCity-Location.csv
mv "$(find . -name "*-Blocks.csv")" ../GeoLiteCity-Blocks.csv
mv "$(find . -name "GeoIP-*.csv")" ../GeoIPCountryWhois.csv
cd ../..
rm -fr tmp/GeoIPDB
mkdir "$(readlink -f data)" 2>/dev/null
cp files/geoip-city6.dat data/geoip-city6.dat # these are 2 IPv6 files we don't really use but our code flies without them
cp files/geoip-country6.dat data/geoip-country6.dat
node scripts/updatedb.js
touch "$(readlink -f data)"
